import pygame
import sys

FPS = 60
W = 700  # ширина экрана
H = 300  # высота экрана
WHITE = (255, 255, 255)
BLUE = (0, 70, 225)

sc = pygame.display.set_mode((W, H))
clock = pygame.time.Clock()

def read_write_file():
    f = open("hi.py", "r")
    print(f.read())
    print('-------------\n and read by lines')

    #прочитали создали список
    with open('hi.py', 'r') as f:
        lines = f.read().splitlines()

    #вывели список
    for line in lines:
        print(line)

    f = open("hi2.py", "a") #режим дозаписи
    f.write("\n Иван 1 \n Пётр 12")
    f.close()

    f = open("hi3.py", "w") #только перезапись
    f.write("\n Иван 1 \n Пётр 12")
    f.close()

# координаты и радиус круга
x = W // 2
y = H // 2
r = 50

while 1:
    for i in pygame.event.get():
        if i.type == pygame.QUIT:
            read_write_file()
            sys.exit()
        elif i.type == pygame.KEYDOWN:
            if i.key == pygame.K_LEFT:
                x -= 30
            elif i.key == pygame.K_RIGHT:
                x += 30

    sc.fill(WHITE)
    pygame.draw.circle(sc, BLUE, (x, y), r)
    pygame.display.update()
    clock.tick(FPS)

# чтение файл
